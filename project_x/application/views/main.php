<!DOCTYPE html>
<html>
<head>
	<title>Pocket</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body">

	<div class="row">
  		<div class="col-md-4"></div>
  		<div class="col-md-4"><h2>Your Pocket List </h2></div>
  		<div class="col-md-4">
  			<ul class="nav nav-tabs">	
				<li role="presentation" class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
				    	<label class="welcome">Tags</label>
				   		<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php foreach ($tags as $x => $x_value):?>
	              				<li><a href="#"><?php echo $x. " " ."($x_value)";?></a></li>
	              		<?php endforeach;?>
					</ul>
	  			</li>
			</ul>
		</div>
	</div>

    <br>
    <br>
    <div class="container">
    	<?php for ($i=0; $i < sizeof($titulos); $i++):?>
	    		<div class="div_lista" >
	    			<h2><?php echo $titulos[$i];?> </h2>
	          		<p><img class="img-responsive" src="<?php echo $total_imgs[$i];?>"></p>
	          		<p></p>
	    		</div>
	    <?php endfor;?>
	    </div>

	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</body>
</html>