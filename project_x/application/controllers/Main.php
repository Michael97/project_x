<?php  
/**
* 
*/
class Main extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index() {

		$this->load->Model('Main_model');
		$data['result'] = $this->Main_model->ObtenerDatos(); 
		
		/**
		Convertir Json a array
		**/
		$json_a = json_decode($this->Main_model->ObtenerDatos(), true);
		$list = $json_a['list'];


		/**
		Obtener los tags, titulos e imagenes de los articulos
		**/
		$titulos = array();
		$imgs = array();
		$tags = array();

		foreach ($list as $list1) {

			$titulos[] = $list1['resolved_title'];
			$imgs[] = $list1['image']['src'];

			foreach ($list1['tags'] as $tag) {
				$tags[] = $tag['tag'];

				}
			}

			$total_tags = array_count_values($tags);
			$data['tags'] = $total_tags; 
			$data['titulos'] = $titulos;
			$data['total_imgs'] = $imgs;

			$this->load->view('main',$data);
	}

}
?>